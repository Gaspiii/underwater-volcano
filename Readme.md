# Underwater Volcano
##### Campsite Reservation Application
## About
The application is written in Java using Spring Boot 2

The database used is an SQL in-memory database using JPA. I chose this because it's easy to demonstrate the capabilities of the actual application 
while 
keeping in mind that swapping to a real persisting database is very easy.

There are integration tests, unit tests and persistence tests written for the application. They all reside in the same location and will all run 
when a mvn clean install is run. Keep in mind that I may have missed some test cases but it was mainly to save on time since it is for an interview
 challenge meant to show code and design capabilities.

I've also added swagger ui to the application to make it easier to run and test it through a browser. You can access it [here][swagger] after 
starting the application.

## Build
To build the application simply run `mvn clean install`

## Execution
To run the application, simply use `mvn spring-boot:run`

## Extra details

### Concurrency
There are locks in place to control the concurrent calls coming for new reservations. I chose this as it was fairly simple. The downsides are that 
we can only have one instance of the application running. We'd need to change it if ever we want to scale horizontally.

### Large volume requests for Availability
The application should be able to handle a large volume of availability requests thanks to the indexes put in place on that table. There are
obviously ways to increase the volume of availability requests the application can handle (i.e. short TTL cache), but I think for this challenge, 
a simple index was enough.


[swagger]: http://localhost:8080/swagger-ui.html#
