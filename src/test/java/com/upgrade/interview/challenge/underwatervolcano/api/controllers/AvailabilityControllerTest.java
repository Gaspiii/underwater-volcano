package com.upgrade.interview.challenge.underwatervolcano.api.controllers;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.convertToLocalDateTime;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.today;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.tomorrow;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static com.upgrade.interview.challenge.underwatervolcano.SwaggerConfig.DATE_TIME_FORMAT;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.upgrade.interview.challenge.underwatervolcano.api.models.AvailabilityResponseEntry;
import com.upgrade.interview.challenge.underwatervolcano.services.AvailabilityRequestService;
import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;
import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;
import com.upgrade.interview.challenge.underwatervolcano.services.AvailabilityService;

@RunWith(SpringRunner.class)
@WebMvcTest(AvailabilityController.class)
public class AvailabilityControllerTest {
  @Autowired
  private MockMvc mvc;

  @MockBean
  private AvailabilityService availabilityService;

  @MockBean
  private AvailabilityRequestService availabilityRequestService;

  @Test
  public void testGetWithInvalidRequest() throws Exception {
    Message message = Message.from("test");

    LocalDate arrivalDate = today();
    LocalDate departureDate = tomorrow();

    when(availabilityRequestService.getMessageIfInvalid(arrivalDate, departureDate)).thenReturn(Optional.of(message));

    mvc.perform(get("/availability")
            .param("arrivalDate", arrivalDate.toString())
            .param("departureDate", departureDate.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.message", is(message.getMessage())));
  }

  @Test
  public void testGetWithNullDepartureDate() throws Exception {
    LocalDate arrivalDate = today();

    when(availabilityRequestService.getMessageIfInvalid(eq(arrivalDate), any())).thenReturn(Optional.empty());
    when(availabilityService.getAvailabilities(eq(arrivalDate), any())).thenReturn(List.of());

    LocalDate expectedDepartureDate = arrivalDate.plusMonths(1);

    mvc.perform(get("/availability")
        .param("arrivalDate", arrivalDate.toString())
        .contentType(MediaType.APPLICATION_JSON));

    verify(availabilityService, times(1)).getAvailabilities(arrivalDate, expectedDepartureDate);
  }

  @Test
  public void testGet() throws Exception {
    LocalDate arrivalDate = today();
    LocalDate departureDate = arrivalDate.plusDays(2);
    LocalDateTime today = convertToLocalDateTime(LocalDate.now());

    when(availabilityRequestService.getMessageIfInvalid(eq(arrivalDate), any())).thenReturn(Optional.empty());
    when(availabilityService.getAvailabilities(any(), any())).thenReturn(List.of(
        new AvailabilityResponseEntry(today, false),
        new AvailabilityResponseEntry(today.plusDays(1), true)
    ));

    mvc.perform(get("/availability")
            .param("arrivalDate", arrivalDate.toString())
            .param("departureDate", departureDate.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].date", is(today.format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)))))
        .andExpect(jsonPath("$[0].isAvailable", is(false)))
        .andExpect(jsonPath("$[1].date", is(today.plusDays(1).format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)))))
        .andExpect(jsonPath("$[1].isAvailable", is(true)));

    verify(availabilityService, times(1)).getAvailabilities(arrivalDate, departureDate);
  }
}