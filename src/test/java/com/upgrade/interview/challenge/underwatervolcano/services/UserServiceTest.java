package com.upgrade.interview.challenge.underwatervolcano.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.upgrade.interview.challenge.underwatervolcano.entities.User;
import com.upgrade.interview.challenge.underwatervolcano.repositories.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
  private static final int CONCURRENT_TEST_SIZE = 20;

  @Mock
  private UserRepository userRepository;

  private UserService sut;

  @Before
  public void setup() {
    sut = new UserService(userRepository);
  }

  @Test
  public void testGetOrCreateExistingUser() {
    String email = "email@email.com";
    String firstName = "first";
    String lastName = "last";

    User expected = User.builder()
        .id(1L)
        .email(email)
        .firstName(firstName)
        .lastName(lastName)
        .build();

    when(userRepository.findUserByEmailAndFirstNameAndLastName(email, firstName, lastName)).thenReturn(Optional.of(expected));

    User actual = sut.getOrCreateUser(email, firstName, lastName);

    Assertions.assertThat(actual).isEqualTo(expected);
    verify(userRepository, never()).save(any());
  }

  @Test
  public void testGetOrCreateNewUser() {
    String email = "email@email.com";
    String firstName = "first";
    String lastName = "last";

    User expected = User.builder()
        .id(1L)
        .email(email)
        .firstName(firstName)
        .lastName(lastName)
        .build();

    when(userRepository.findUserByEmailAndFirstNameAndLastName(email, firstName, lastName)).thenReturn(Optional.empty());
    when(userRepository.save(any())).thenReturn(expected);

    User actual = sut.getOrCreateUser(email, firstName, lastName);

    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void testConcurrentCreateUser() throws InterruptedException {
    BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(CONCURRENT_TEST_SIZE);
    Semaphore testsRun = new Semaphore(0);

    when(userRepository.findUserByEmailAndFirstNameAndLastName(any(), any(), any())).thenAnswer(new Answer<Optional<User>>() {
      private AtomicInteger count = new AtomicInteger(0);
      @Override public Optional<User> answer(InvocationOnMock invocation) throws Throwable {
        if(count.incrementAndGet() == 1) {
          Thread.sleep(1000);
          return Optional.empty();
        }
        return Optional.of(mock(User.class));
      }
    });

    when(userRepository.save(any())).thenReturn(mock(User.class));

    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(CONCURRENT_TEST_SIZE, CONCURRENT_TEST_SIZE, 1, TimeUnit.SECONDS, queue);
    for(int i=0; i<CONCURRENT_TEST_SIZE; i++) {
      threadPoolExecutor.execute(() -> {
        sut.getOrCreateUser("email@email.com", "first", "last");
        testsRun.release();
      });
    }

    boolean acquired = testsRun.tryAcquire(CONCURRENT_TEST_SIZE, 10, TimeUnit.SECONDS);
    Assertions.assertThat(acquired).isTrue();

    verify(userRepository, times(1)).save(any());
  }
}