package com.upgrade.interview.challenge.underwatervolcano.repositories;

import java.time.LocalDate;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;
import com.upgrade.interview.challenge.underwatervolcano.entities.User;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ReservationRepositoryTest {
  @Autowired
  private ReservationRepository sut;

  @Autowired
  private UserRepository userRepository;

  @Before
  public void setup() {
    sut.deleteAll();
    userRepository.deleteAll();
  }

  @After
  public void after() {
    sut.deleteAll();
    userRepository.deleteAll();
  }

  @Test
  public void shouldNotFindReservationStartingAndEndingBefore() {
    LocalDate reservationArrivalDate = LocalDate.now();
    LocalDate reservedDepartureDate = reservationArrivalDate.plusDays(5);
    createAndSaveReservation(reservationArrivalDate, reservedDepartureDate);

    LocalDate arrivalDate = reservedDepartureDate.plusDays(1);
    LocalDate departureDate = arrivalDate.plusDays(5);

    List<Reservation> actual = sut.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);

    Assertions.assertThat(actual).isEmpty();
  }

  @Test
  public void shouldNotFindReservationStartingAndEndingAfter() {
    LocalDate reservationArrivalDate = LocalDate.now();
    LocalDate reservedDepartureDate = reservationArrivalDate.plusDays(5);
    createAndSaveReservation(reservationArrivalDate, reservedDepartureDate);

    LocalDate arrivalDate = reservedDepartureDate.plusDays(1);
    LocalDate departureDate = arrivalDate.plusDays(5);

    List<Reservation> actual = sut.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);

    Assertions.assertThat(actual).isEmpty();
  }

  @Test
  public void shouldFindReservationStartingBeforeAndEndingDuring() {
    LocalDate reservationArrivalDate = LocalDate.now();
    LocalDate reservedDepartureDate = reservationArrivalDate.plusDays(5);
    Reservation expected = createAndSaveReservation(reservationArrivalDate, reservedDepartureDate);

    LocalDate arrivalDate = reservationArrivalDate.plusDays(1);
    LocalDate departureDate = reservedDepartureDate.plusDays(1);

    List<Reservation> actual = sut.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);

    Assertions.assertThat(actual).containsOnly(expected);
  }

  @Test
  public void shouldFindReservationStartingDuringAndEndingAfter() {
    LocalDate reservationArrivalDate = LocalDate.now();
    LocalDate reservedDepartureDate = reservationArrivalDate.plusDays(5);
    Reservation expected = createAndSaveReservation(reservationArrivalDate, reservedDepartureDate);

    LocalDate arrivalDate = reservationArrivalDate.minusDays(1);
    LocalDate departureDate = reservedDepartureDate.minusDays(1);

    List<Reservation> actual = sut.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);

    Assertions.assertThat(actual).containsOnly(expected);
  }

  @Test
  public void shouldFindReservationStartingBeforeAndEndingAfter() {
    LocalDate reservationArrivalDate = LocalDate.now();
    LocalDate reservedDepartureDate = reservationArrivalDate.plusDays(5);
    Reservation expected = createAndSaveReservation(reservationArrivalDate, reservedDepartureDate);

    LocalDate arrivalDate = reservationArrivalDate.plusDays(1);
    LocalDate departureDate = reservedDepartureDate.minusDays(1);

    List<Reservation> actual = sut.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);

    Assertions.assertThat(actual).containsOnly(expected);
  }

  @Test
  public void shouldFindReservationStartingDuringAndEndingDuring() {
    LocalDate reservationArrivalDate = LocalDate.now();
    LocalDate reservedDepartureDate = reservationArrivalDate.plusDays(5);
    Reservation expected = createAndSaveReservation(reservationArrivalDate, reservedDepartureDate);

    LocalDate arrivalDate = reservationArrivalDate.minusDays(1);
    LocalDate departureDate = reservedDepartureDate.plusDays(1);

    List<Reservation> actual = sut.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);

    Assertions.assertThat(actual).containsOnly(expected);
  }

  private Reservation createAndSaveReservation(LocalDate arrivalDate, LocalDate departureDate) {
    Reservation reservation = createReservation(arrivalDate, departureDate);
    return sut.save(reservation);
  }

  private Reservation createReservation(LocalDate arrivalDate, LocalDate departureDate) {
    return Reservation.builder()
        .arrivalDate(arrivalDate)
        .departureDate(departureDate)
        .user(createAndSaveUser())
        .build();
  }

  private User createAndSaveUser() {
    return userRepository.save(createUser());
  }

  private static User createUser() {
    return User.builder()
        .email("some@email.com")
        .firstName("first")
        .lastName("last")
        .build();
  }
}