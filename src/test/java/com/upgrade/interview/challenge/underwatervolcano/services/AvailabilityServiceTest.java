package com.upgrade.interview.challenge.underwatervolcano.services;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.convertToLocalDateTime;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.today;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.upgrade.interview.challenge.underwatervolcano.api.models.AvailabilityResponseEntry;
import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityServiceTest {
  @Mock
  private ReservationService reservationRepository;

  private AvailabilityService sut;

  @Before
  public void setup() {
    sut = new AvailabilityService(reservationRepository);
  }
  
  @Test
  public void testGetAvailabilities() {
    LocalDate arrivalDate = today();
    LocalDate departureDate = arrivalDate.plusDays(3);
    when(reservationRepository.getReservationsWithUnionToBookingDates(arrivalDate, departureDate)).thenReturn(List.of(
        createReservation(arrivalDate),
        createReservation(arrivalDate.plusDays(2))));

    List<AvailabilityResponseEntry> expected = List.of(
        new AvailabilityResponseEntry(convertToLocalDateTime(arrivalDate), false),
        new AvailabilityResponseEntry(convertToLocalDateTime(arrivalDate.plusDays(1)), true),
        new AvailabilityResponseEntry(convertToLocalDateTime(arrivalDate.plusDays(2)), false));

    List<AvailabilityResponseEntry> actual = sut.getAvailabilities(arrivalDate, departureDate);

    Assertions.assertThat(actual).containsExactlyElementsOf(expected);
  }

  private Reservation createReservation(LocalDate arrivalDate) {
    return Reservation.builder()
        .arrivalDate(arrivalDate)
        .departureDate(arrivalDate.plusDays(1))
        .build();
  }
}

