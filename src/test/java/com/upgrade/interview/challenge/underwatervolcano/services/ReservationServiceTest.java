package com.upgrade.interview.challenge.underwatervolcano.services;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.today;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.tomorrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;
import com.upgrade.interview.challenge.underwatervolcano.entities.User;
import com.upgrade.interview.challenge.underwatervolcano.repositories.ReservationRepository;

@RunWith(MockitoJUnitRunner.class)
public class ReservationServiceTest {
  private static final int CONCURRENT_TEST_SIZE = 20;
  @Mock
  private ReservationRepository reservationRepository;

  @Mock
  private UserService userService;

  private ReservationService sut;

  @Before
  public void setup() {
    sut = new ReservationService(reservationRepository, userService);
  }

  @Test
  public void testBooking() {
    Long userId = 1L;
    String email = "random@email.com";
    String firstName = "first";
    String lastName = "last";
    LocalDate arrival = LocalDate.now();
    LocalDate departure = arrival.plusDays(1);

    User user = User.builder()
        .id(userId)
        .email(email)
        .firstName(firstName)
        .lastName(lastName)
        .build();

    Reservation expected = mock(Reservation.class);

    when(userService.getOrCreateUser(email, firstName, lastName)).thenReturn(user);
    when(reservationRepository.save(any())).thenReturn(expected);

    Optional<Reservation> actual = sut.book(email, firstName, lastName, arrival, departure);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualTo(expected);
  }

  @Test
  public void testUpdateReservation() {
    Long userId = 123L;
    String email = "random@email.com";
    String firstName = "first";
    String lastName = "last";
    LocalDate arrival = LocalDate.now();
    LocalDate departure = arrival.plusDays(1);

    User user = User.builder()
        .id(userId)
        .email("old@email.com")
        .firstName("oldFirst")
        .lastName("oldLast")
        .build();

    Reservation reservation = Reservation.builder()
        .id(1L)
        .arrivalDate(LocalDate.now().plusDays(10))
        .departureDate(LocalDate.now().plusDays(15))
        .user(user)
        .build();

    Reservation expected = Reservation.builder()
        .id(reservation.getId())
        .user(User.builder()
            .id(userId)
            .email(email)
            .firstName(firstName)
            .lastName(lastName)
            .build())
        .arrivalDate(arrival)
        .departureDate(departure)
        .build();

    when(reservationRepository.save(any())).thenReturn(expected);

    Optional<Reservation> actual = sut.update(reservation, email, firstName, lastName, arrival, departure);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualTo(expected);

    ArgumentCaptor<Reservation> captor = ArgumentCaptor.forClass(Reservation.class);
    verify(reservationRepository, times(1)).save(captor.capture());
    Assertions.assertThat(captor.getValue()).isEqualToComparingFieldByFieldRecursively(expected);
  }

  @Test
  public void testDeleteReservation() {
    sut.delete(1L);

    verify(reservationRepository, times(1)).deleteById(1L);
  }

  @Test
  public void testGetReservation() {
    sut.get(1L);

    verify(reservationRepository, times(1)).findById(1L);
  }

  @Test
  public void testIsBookable() {
    LocalDate arrivalDate = today();
    LocalDate departureDate = tomorrow();

    when(reservationRepository.findReservationsWithUnionToBookingDates(arrivalDate, departureDate)).thenReturn(List.of());

    Boolean actual = sut.isBookable(arrivalDate, departureDate);

    Assertions.assertThat(actual).isTrue();
  }

  @Test
  public void testConcurrentSaveRequests() throws InterruptedException {
    User user = User.builder()
        .id(1L)
        .email("email@email.com")
        .firstName("first")
        .lastName("last")
        .build();
    when(userService.getOrCreateUser(any(), any(), any())).thenReturn(user);

    BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(CONCURRENT_TEST_SIZE);
    Semaphore testsRun = new Semaphore(0);

    when(reservationRepository.findReservationsWithUnionToBookingDates(any(), any())).thenAnswer(new Answer<List<Reservation>>() {
      private AtomicInteger count = new AtomicInteger(0);

      @Override public List<Reservation> answer(InvocationOnMock invocation) throws Throwable {
        if(count.incrementAndGet() == 1) {
          Thread.sleep(1000);
          return List.of();
        }
        return List.of(mock(Reservation.class));
      }
    });

    when(reservationRepository.save(any())).thenReturn(mock(Reservation.class));

    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(CONCURRENT_TEST_SIZE, CONCURRENT_TEST_SIZE, 1, TimeUnit.SECONDS, queue);
    for(int i=0; i<CONCURRENT_TEST_SIZE; i++) {
      threadPoolExecutor.execute(() -> {
        sut.book("email@email.com", "first", "last", tomorrow(), tomorrow().plusDays(3));
        testsRun.release();
      });
    }

    boolean acquired = testsRun.tryAcquire(CONCURRENT_TEST_SIZE, 10, TimeUnit.SECONDS);
    Assertions.assertThat(acquired).isTrue();

    verify(reservationRepository, times(1)).save(any());
  }
}