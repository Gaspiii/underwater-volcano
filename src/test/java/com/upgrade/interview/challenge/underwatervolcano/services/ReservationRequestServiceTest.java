package com.upgrade.interview.challenge.underwatervolcano.services;

import java.time.LocalDate;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;
import com.upgrade.interview.challenge.underwatervolcano.api.models.ReservationRequest;

public class ReservationRequestServiceTest {
  private ReservationRequestService sut;

  @Before
  public void setup() {
    sut = new ReservationRequestService();
  }

  @Test
  public void shouldReturnNullWithProperDates() {
    LocalDate arrivalDate = LocalDate.now().plusDays(1);
    LocalDate departureDate = arrivalDate.plusDays(3);
    ReservationRequest reservationRequest = createReservationRequest(arrivalDate, departureDate);

    Optional<Message> actual = sut.getMessageIfInvalid(reservationRequest);

    Assertions.assertThat(actual.isPresent()).isFalse();
  }

  @Test
  public void shouldReturnMessageForArrivalDateInThePast() {
    LocalDate arrivalDate = LocalDate.now();
    LocalDate departureDate = arrivalDate.plusDays(3);
    ReservationRequest reservationRequest = createReservationRequest(arrivalDate, departureDate);

    Optional<Message> actual = sut.getMessageIfInvalid(reservationRequest);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualToComparingFieldByField(Message.from("Invalid arrival date. It must be set to a day after today."));
  }

  @Test
  public void shouldReturnMessageForArrivalDateTooFarInFuture() {
    LocalDate arrivalDate = LocalDate.now().plusMonths(1).plusDays(1);
    LocalDate departureDate = arrivalDate.plusDays(3);
    ReservationRequest reservationRequest = createReservationRequest(arrivalDate, departureDate);

    Optional<Message> actual = sut.getMessageIfInvalid(reservationRequest);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualToComparingFieldByField(Message.from("Invalid arrival date. Bookings more than a month away are not accepted."));
  }

  @Test
  public void shouldReturnMessageForDepartureDateBeforeOrOnArrivalDate() {
    LocalDate arrivalDate = LocalDate.now().plusDays(1);
    LocalDate departureDate = arrivalDate;
    ReservationRequest reservationRequest = createReservationRequest(arrivalDate, departureDate);

    Optional<Message> actual = sut.getMessageIfInvalid(reservationRequest);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualToComparingFieldByField(Message.from("Invalid departure date. It must occur after the chosen arrival date."));
  }

  @Test
  public void shouldReturnMessageForReservationLengthTooLong() {
    LocalDate arrivalDate = LocalDate.now().plusDays(1);
    LocalDate departureDate = arrivalDate.plusDays(4);
    ReservationRequest reservationRequest = createReservationRequest(arrivalDate, departureDate);

    Optional<Message> actual = sut.getMessageIfInvalid(reservationRequest);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualToComparingFieldByField(Message.from("Reservation range is too long. Maximum booked days is 3."));
  }

  private ReservationRequest createReservationRequest(LocalDate arrivalDate, LocalDate departureDate) {
    return ReservationRequest.builder()
        .email("some@email.com")
        .firstName("first")
        .lastName("last")
        .arrivalDate(arrivalDate)
        .departureDate(departureDate)
        .build();
  }
}