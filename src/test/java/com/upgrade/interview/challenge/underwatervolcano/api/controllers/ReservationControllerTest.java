package com.upgrade.interview.challenge.underwatervolcano.api.controllers;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.today;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.tomorrow;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Optional;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;
import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;
import com.upgrade.interview.challenge.underwatervolcano.entities.User;
import com.upgrade.interview.challenge.underwatervolcano.services.ReservationRequestService;
import com.upgrade.interview.challenge.underwatervolcano.services.ReservationService;

@RunWith(SpringRunner.class)
@WebMvcTest(ReservationController.class)
public class ReservationControllerTest {
  @Autowired
  private MockMvc mvc;

  @MockBean
  private ReservationService reservationService;

  @MockBean ReservationRequestService reservationRequestService;

  /**** BOOK ***/

  @Test
  public void testBookingWithInvalidRequest() throws Exception {
    JSONObject request = createReservationRequest();
    Message message = Message.from("test");
    when(reservationRequestService.getMessageIfInvalid(any())).thenReturn(Optional.of(message));

    mvc.perform(post("/reservation")
            .content(request.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.message", is(message.getMessage())));

    verifyZeroInteractions(reservationService);
  }

  @Test
  public void testBookingWithDatesAlreadyReserved() throws Exception {
    JSONObject request = createReservationRequest();
    when(reservationRequestService.getMessageIfInvalid(any())).thenReturn(Optional.empty());
    when(reservationService.isBookable(any(), any())).thenReturn(Boolean.FALSE);

    mvc.perform(post("/reservation")
            .contentType(MediaType.APPLICATION_JSON)
            .content(request.toString()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Reservation not accepted. Date was already pre-reserved.")));
  }

  @Test
  public void testBooking() throws Exception {
    JSONObject request = createReservationRequest();
    User user = convertRequestJsonToUser(request);
    Reservation reservation = convertRequestJsonToReservation(request, user);

    when(reservationRequestService.getMessageIfInvalid(any())).thenReturn(Optional.empty());
    when(reservationService.isBookable(reservation.getArrivalDate(), reservation.getDepartureDate())).thenReturn(Boolean.TRUE);
    when(reservationService.book(user.getEmail(), user.getFirstName(), user.getLastName(), reservation.getArrivalDate(), reservation.getDepartureDate())).thenReturn(Optional.of(reservation));

    mvc.perform(post("/reservation")
            .content(request.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", is(100)));
  }

  /**** UPDATE ***/

  @Test
  public void testUpdateWithInvalidRequest() throws Exception {
    JSONObject request = createReservationRequest();
    Message message = Message.from("test");
    when(reservationRequestService.getMessageIfInvalid(any())).thenReturn(Optional.of(message));

    mvc.perform(put("/reservation/1")
            .content(request.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.message", is(message.getMessage())));

    verifyZeroInteractions(reservationService);
  }

  @Test
  public void testUpdateForAReservationNotFound() throws Exception {
    JSONObject request = createReservationRequest();
    when(reservationRequestService.getMessageIfInvalid(any())).thenReturn(Optional.empty());
    when(reservationService.get(1L)).thenReturn(Optional.empty());

    mvc.perform(put("/reservation/1")
            .content(request.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Cannot find a reservation with the given ID.")));
  }

  @Test
  public void testUpdateWithDatesAlreadyReserved() throws Exception {
    JSONObject request = createReservationRequest();
    User user = convertRequestJsonToUser(request);
    Reservation reservation = convertRequestJsonToReservation(request, user);

    when(reservationRequestService.getMessageIfInvalid(any())).thenReturn(Optional.empty());
    when(reservationService.get(100L)).thenReturn(Optional.of(reservation));
    when(reservationService.isBookable(any(), any(), eq(100L))).thenReturn(Boolean.FALSE);

    mvc.perform(put("/reservation/100")
            .content(request.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Reservation not accepted. Date was already pre-reserved.")));
  }

  @Test
  public void testUpdate() throws Exception {
    JSONObject request = createReservationRequest();
    User user = convertRequestJsonToUser(request);
    Reservation reservation = convertRequestJsonToReservation(request, user);

    when(reservationRequestService.getMessageIfInvalid(any())).thenReturn(Optional.empty());
    when(reservationService.get(100L)).thenReturn(Optional.of(reservation));
    when(reservationService.isBookable(any(), any(), eq(100L))).thenReturn(Boolean.TRUE);
    when(reservationService.update(eq(reservation), any(), any(), any(), any(), any())).thenReturn(Optional.of(reservation));

    mvc.perform(put("/reservation/100")
            .content(request.toString())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(100)));
  }

  /**** GET ***/

  @Test
  public void testGetForAReservationNotFound() throws Exception {
    when(reservationService.get(1L)).thenReturn(Optional.empty());

    mvc.perform(get("/reservation/1")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Cannot find a reservation with the given ID.")));
  }

  @Test
  public void testGet() throws Exception {
    Reservation reservation = Reservation.builder()
        .id(1L)
        .arrivalDate(today())
        .departureDate(today())
        .build();
    when(reservationService.get(1L)).thenReturn(Optional.of(reservation));

    mvc.perform(get("/reservation/1")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(1)));
  }

  /**** CANCEL ***/

  @Test
  public void testCancelForAReservationNotFound() throws Exception {
    when(reservationService.get(1L)).thenReturn(Optional.empty());

    mvc.perform(delete("/reservation/1")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Cannot find a reservation with the given ID.")));
  }

  @Test
  public void testCancelForReservationInPast() throws Exception {
    Reservation reservation = Reservation.builder()
        .id(1L)
        .arrivalDate(today().minusDays(1))
        .departureDate(today())
        .build();
    when(reservationService.get(1L)).thenReturn(Optional.of(reservation));

    mvc.perform(delete("/reservation/1")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Reservation already past, therefore it is not deletable.")));
  }

  @Test
  public void testCancel() throws Exception {
    Reservation reservation = Reservation.builder()
        .id(1L)
        .arrivalDate(today())
        .departureDate(today().plusDays(1))
        .build();
    when(reservationService.get(1L)).thenReturn(Optional.of(reservation));

    mvc.perform(delete("/reservation/1")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Reservation was deleted")));
  }

  private static JSONObject createReservationRequest() throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("email", "some@email.com");
    jsonObject.put("firstName", "first");
    jsonObject.put("lastName", "last");
    jsonObject.put("arrivalDate", today());
    jsonObject.put("departureDate", tomorrow());
    return jsonObject;
  }

  private static User convertRequestJsonToUser(JSONObject jsonObject) throws JSONException {
    return User.builder()
        .id(1L)
        .email(jsonObject.getString("email"))
        .firstName(jsonObject.getString("firstName"))
        .lastName(jsonObject.getString("lastName"))
        .build();
  }

  private static Reservation convertRequestJsonToReservation(JSONObject jsonObject, User user) throws JSONException {
    return Reservation.builder()
        .id(100L)
        .arrivalDate(LocalDate.parse(jsonObject.getString("arrivalDate")))
        .departureDate(LocalDate.parse(jsonObject.getString("departureDate")))
        .user(user)
        .build();
  }
}