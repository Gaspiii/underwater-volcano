package com.upgrade.interview.challenge.underwatervolcano.integration;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.today;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.tomorrow;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.assertj.core.api.Assertions;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.upgrade.interview.challenge.underwatervolcano.UnderwaterVolcanoApplication;
import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;
import com.upgrade.interview.challenge.underwatervolcano.entities.User;
import com.upgrade.interview.challenge.underwatervolcano.repositories.ReservationRepository;
import com.upgrade.interview.challenge.underwatervolcano.repositories.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = MOCK, classes = UnderwaterVolcanoApplication.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ReservationControllerIntegrationTest {
  private static final int CONCURRENT_TEST_SIZE = 20;

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ReservationRepository reservationRepository;

  @Autowired
  private UserRepository userRepository;

  @Test
  public void testGetReservation() throws Exception {
    User user = createUser("email@email.com", "first", "last");
    user = persistUser(user);
    Reservation reservation = createReservation(today(), tomorrow(), user);
    reservation = persistReservation(reservation);

    mvc.perform(get("/reservation/" + reservation.getId())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(reservation.getId().intValue())));
  }

  @Test
  public void testReserveForMoreThanThreeDays() throws Exception {
    LocalDate arrivalDate = tomorrow();
    LocalDate departureDate = arrivalDate.plusDays(4);
    JSONObject request = createReservationRequestJson("email@email.com", "first", "last", arrivalDate, departureDate);

    mvc.perform(post("/reservation")
            .contentType(MediaType.APPLICATION_JSON)
            .content(request.toString()))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is("Reservation range is too long. Maximum booked days is 3.")));
  }

  @Test
  public void testReserveLessThanOneDayInAdvance() throws Exception {
    LocalDate arrivalDate = today();
    LocalDate departureDate = arrivalDate.plusDays(1);
    JSONObject request = createReservationRequestJson("email@email.com", "first", "last", arrivalDate, departureDate);

    mvc.perform(post("/reservation")
        .contentType(MediaType.APPLICATION_JSON)
        .content(request.toString()))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is("Invalid arrival date. It must be set to a day after today.")));
  }

  @Test
  public void testReserveMoreThanAMonthInAdvance() throws Exception {
    LocalDate arrivalDate = today().plusMonths(1).plusDays(1);
    LocalDate departureDate = arrivalDate.plusDays(1);
    JSONObject request = createReservationRequestJson("email@email.com", "first", "last", arrivalDate, departureDate);

    mvc.perform(post("/reservation")
            .contentType(MediaType.APPLICATION_JSON)
            .content(request.toString()))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is("Invalid arrival date. Bookings more than a month away are not accepted.")));
  }

  @Test
  public void testCancellingReservation() throws Exception {
    User user = createUser("email@email.com", "first", "last");
    user = persistUser(user);
    Reservation reservation = createReservation(today(), tomorrow(), user);
    reservation = persistReservation(reservation);

    mvc.perform(delete("/reservation/" + reservation.getId())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is("Reservation was deleted")));
  }

  @Test
  public void testBookingReservation() throws Exception {
    LocalDate arrivalDate = tomorrow();
    LocalDate departureDate = arrivalDate.plusDays(1);
    JSONObject request = createReservationRequestJson("email@email.com", "first", "last", arrivalDate, departureDate);

    mvc.perform(post("/reservation")
            .contentType(MediaType.APPLICATION_JSON)
            .content(request.toString()))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", instanceOf(Integer.class)));
  }

  @Test
  public void testModifyingReservation() throws Exception {
    LocalDate arrivalDate = tomorrow();
    LocalDate departureDate = arrivalDate.plusDays(1);

    User user = createUser("email@email.com", "first", "last");
    user = userRepository.save(user);

    Reservation reservation = createReservation(arrivalDate, departureDate, user);
    reservation = reservationRepository.save(reservation);

    JSONObject request = createReservationRequestJson("newEmail@email.com", "newFirst", "newLast", arrivalDate.plusDays(1), departureDate.plusDays(3));

    mvc.perform(put("/reservation/" + reservation.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(request.toString()))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", instanceOf(Integer.class)));
  }
  
  @Test
  public void testConcurrentReservations() throws InterruptedException, JSONException {
    BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(CONCURRENT_TEST_SIZE);
    Semaphore testsRun = new Semaphore(0);

    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(CONCURRENT_TEST_SIZE, CONCURRENT_TEST_SIZE, 1, TimeUnit.SECONDS, queue);
    LocalDate arrivalDate = tomorrow();
    LocalDate departureDate = arrivalDate.plusDays(1);
    JSONObject request = createReservationRequestJson("email@email.com", "first", "last", arrivalDate, departureDate);

    for(int i=0; i<CONCURRENT_TEST_SIZE; i++) {
      threadPoolExecutor.execute(() -> {
        try {
          mvc.perform(post("/reservation")
              .contentType(MediaType.APPLICATION_JSON)
              .content(request.toString()));
        } catch (Exception e) {
          Assertions.fail("Error thrown when it should not have.");
        } finally {
          testsRun.release();
        }
      });
    }

    boolean acquired = testsRun.tryAcquire(CONCURRENT_TEST_SIZE, 10, TimeUnit.SECONDS);
    Assertions.assertThat(acquired).isTrue();
    Assertions.assertThat(userRepository.count()).isEqualTo(1L);
    Assertions.assertThat(reservationRepository.count()).isEqualTo(1L);
  }

  private User persistUser(User user) {
    return userRepository.save(user);
  }

  private Reservation persistReservation(Reservation reservation) {
    return reservationRepository.save(reservation);
  }

  private User createUser(String email, String firstName, String lastName) {
    return User.builder()
        .email(email)
        .firstName(firstName)
        .lastName(lastName)
        .build();
  }

  private Reservation createReservation(LocalDate arrivalDate, LocalDate departureDate, User user) {
    return Reservation.builder()
        .arrivalDate(arrivalDate)
        .departureDate(departureDate)
        .user(user)
        .build();
  }

  private JSONObject createReservationRequestJson(String email, String firstName, String lastName, LocalDate arrivalDate, LocalDate departureDate) throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("email", email);
    jsonObject.put("firstName", firstName);
    jsonObject.put("lastName", lastName);
    jsonObject.put("arrivalDate", arrivalDate);
    jsonObject.put("departureDate", departureDate);
    return jsonObject;
  }
}
