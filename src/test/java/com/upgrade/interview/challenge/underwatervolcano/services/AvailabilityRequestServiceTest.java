package com.upgrade.interview.challenge.underwatervolcano.services;

import java.time.LocalDate;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;
import com.upgrade.interview.challenge.underwatervolcano.services.AvailabilityRequestService;

public class AvailabilityRequestServiceTest {
  private AvailabilityRequestService sut;

  @Before
  public void setup() {
    this.sut = new AvailabilityRequestService();
  }

  @Test
  public void shouldReturnNullWithProperDates() {
    LocalDate arrivalDate = LocalDate.now().plusMonths(1);
    LocalDate departureDate = arrivalDate.plusMonths(1);

    Optional<Message> actual = sut.getMessageIfInvalid(arrivalDate, departureDate);

    Assertions.assertThat(actual.isPresent()).isFalse();
  }

  @Test
  public void shouldReturnMessageForInvalidArrivalDate() {
    LocalDate arrivalDate = LocalDate.now();
    LocalDate departureDate = arrivalDate.plusMonths(1);

    Message expected = Message.from("Invalid arrival date. It must be set to any day in the future.");

    Optional<Message> actual = sut.getMessageIfInvalid(arrivalDate, departureDate);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualToComparingFieldByField(expected);
  }

  @Test
  public void shouldReturnMessageForInvalidDepartureDate() {
    LocalDate arrivalDate = LocalDate.now().plusMonths(1);
    LocalDate departureDate = arrivalDate.minusDays(1);

    Message expected = Message.from("Invalid departure date. It must occur after the chosen arrival date.");

    Optional<Message> actual = sut.getMessageIfInvalid(arrivalDate, departureDate);

    Assertions.assertThat(actual.isPresent()).isTrue();
    Assertions.assertThat(actual.get()).isEqualToComparingFieldByField(expected);
  }
}