package com.upgrade.interview.challenge.underwatervolcano.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
  @Query("FROM Reservation " +
      "WHERE ((arrivalDate BETWEEN ?1 AND ?2) OR " +
      "(departureDate BETWEEN ?1 AND ?2) OR " +
      "(?1 BETWEEN arrivalDate AND departureDate) OR " +
      "(?2 BETWEEN arrivalDate AND departureDate)) AND " +
      "departureDate != ?1 AND " +
      "arrivalDate != ?2")
  List<Reservation> findReservationsWithUnionToBookingDates(LocalDate arrivalDate, LocalDate departureDate);
}
