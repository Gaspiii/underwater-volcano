package com.upgrade.interview.challenge.underwatervolcano.api.controllers;

import static com.upgrade.interview.challenge.underwatervolcano.SwaggerConfig.DATE_FORMAT;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.convertToLocalDateTime;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.upgrade.interview.challenge.underwatervolcano.services.AvailabilityRequestService;
import com.upgrade.interview.challenge.underwatervolcano.api.models.AvailabilityResponseEntry;
import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;
import com.upgrade.interview.challenge.underwatervolcano.services.AvailabilityService;

@RestController
@RequestMapping(path = "/availability", produces = "application/json")
@RequiredArgsConstructor
public class AvailabilityController {
  @NonNull
  private final AvailabilityService availabilityService;

  @NonNull
  private final AvailabilityRequestService availabilityRequestService;

  @ApiOperation(value = "Shows a list of dates with their availabilities")
  @RequestMapping(method = RequestMethod.GET)
  @ApiResponses(value = {
      @ApiResponse(code = 200, response = AvailabilityResponseEntry[].class, message = "Successfully retrieved list of availabilities."),
      @ApiResponse(code = 400, response = Message.class, message = "Invalid arrival and/or departure date."),
  })
  public ResponseEntity get(
      @RequestParam(value="arrivalDate") @DateTimeFormat(pattern=DATE_FORMAT) LocalDate arrivalDate,
      @RequestParam(value = "departureDate", required = false) @DateTimeFormat(pattern=DATE_FORMAT) LocalDate departureDate) {
    if(departureDate == null) {
      departureDate = arrivalDate.plusMonths(1);
    }

    Optional<Message> invalidRequestMessage = availabilityRequestService.getMessageIfInvalid(arrivalDate, departureDate);
    if(invalidRequestMessage.isPresent()) {
      return ResponseEntity.badRequest().body(invalidRequestMessage);
    }

    List<AvailabilityResponseEntry> availabilityResponseEntries = availabilityService.getAvailabilities(arrivalDate, departureDate);

    return ResponseEntity.ok(availabilityResponseEntries);
  }
}
