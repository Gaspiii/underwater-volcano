package com.upgrade.interview.challenge.underwatervolcano.services;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.today;

import java.time.LocalDate;
import java.util.Optional;

import lombok.NoArgsConstructor;

import org.springframework.stereotype.Service;

import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;

@Service
@NoArgsConstructor
public class AvailabilityRequestService {
  public Optional<Message> getMessageIfInvalid(LocalDate arrivalDate, LocalDate departureDate) {
    Message message = null;

    if(!arrivalDateIsAfterToday(arrivalDate)) {
      message = Message.from("Invalid arrival date. It must be set to any day in the future.");
    }

    if(!departureDateIsAfterArrivalDate(arrivalDate, departureDate)) {
      message = Message.from("Invalid departure date. It must occur after the chosen arrival date.");
    }

    return Optional.ofNullable(message);
  }

  private static boolean arrivalDateIsAfterToday(LocalDate arrivalDate) {
    return arrivalDate.isAfter(today());
  }

  private static boolean departureDateIsAfterArrivalDate(LocalDate arrivalDate, LocalDate departureDate) {
    return departureDate.isAfter(arrivalDate);
  }
}
