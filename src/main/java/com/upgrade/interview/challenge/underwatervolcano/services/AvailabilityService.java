package com.upgrade.interview.challenge.underwatervolcano.services;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.convertToLocalDateTime;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import com.upgrade.interview.challenge.underwatervolcano.api.models.AvailabilityResponseEntry;
import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;

@Service
@RequiredArgsConstructor
public class AvailabilityService {
  @NonNull
  private final ReservationService reservationService;

  public List<AvailabilityResponseEntry> getAvailabilities(LocalDate arrivalDate, LocalDate departureDate) {
    List<Reservation> reservations = reservationService.getReservationsWithUnionToBookingDates(arrivalDate, departureDate);
    return buildAvailabilities(reservations, arrivalDate, departureDate);
  }

  private List<AvailabilityResponseEntry> buildAvailabilities(List<Reservation> reservations, LocalDate arrivalDate, LocalDate departureDate) {
    List<AvailabilityResponseEntry> availabilityResponseEntries = new ArrayList<>();

    LocalDate nextDate = arrivalDate;
    while(nextDate.isBefore(departureDate)) {
      LocalDate localDate = nextDate;
      boolean dateIsReserved = reservations.stream().anyMatch(reservation -> reservedDateMatcher(reservation, localDate));

      availabilityResponseEntries.add(new AvailabilityResponseEntry(convertToLocalDateTime(localDate), !dateIsReserved));
      nextDate = nextDate.plusDays(1);
    }

    return availabilityResponseEntries;
  }

  private static boolean reservedDateMatcher(Reservation reservation, LocalDate date) {
    return !reservation.getArrivalDate().isAfter(date) && reservation.getDepartureDate().isAfter(date);
  }
}
