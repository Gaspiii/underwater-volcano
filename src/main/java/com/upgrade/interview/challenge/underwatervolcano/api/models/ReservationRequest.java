package com.upgrade.interview.challenge.underwatervolcano.api.models;

import static com.upgrade.interview.challenge.underwatervolcano.SwaggerConfig.DATE_FORMAT;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonFormat;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReservationRequest {
  @NonNull
  private String email;

  @NonNull
  private String firstName;

  @NonNull
  private String lastName;

  @NonNull
  @JsonFormat(pattern = DATE_FORMAT)
  private LocalDate arrivalDate;

  @NonNull
  @JsonFormat(pattern = DATE_FORMAT)
  private LocalDate departureDate;
}
