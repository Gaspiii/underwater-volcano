package com.upgrade.interview.challenge.underwatervolcano.api.models;

import lombok.Data;
import lombok.NonNull;

@Data
public class ReservationResponse {
  @NonNull
  private Long id;
}
