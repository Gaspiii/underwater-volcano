package com.upgrade.interview.challenge.underwatervolcano.services;

import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.oneMonthIntoFuture;
import static com.upgrade.interview.challenge.underwatervolcano.utils.DateTimeUtils.today;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;
import com.upgrade.interview.challenge.underwatervolcano.api.models.ReservationRequest;

@Service
public class ReservationRequestService {
  @JsonIgnore
  public Optional<Message> getMessageIfInvalid(ReservationRequest reservationRequest) {
    Message message = null;

    if(reservationRequest.getEmail() == null ||
        reservationRequest.getFirstName() == null ||
        reservationRequest.getLastName() == null ||
        reservationRequest.getArrivalDate() == null ||
        reservationRequest.getDepartureDate() == null) {
      message = Message.from("Invalid reservation request.");
    }

    if(!arrivalDateIsAfterToday(reservationRequest)) {
      message = Message.from("Invalid arrival date. It must be set to a day after today.");
    }

    if(!arrivalDateIsLatestAMonthFromNow(reservationRequest)) {
      message = Message.from("Invalid arrival date. Bookings more than a month away are not accepted.");
    }

    if(!lengthOfReservationIsMaxThreeDays(reservationRequest)) {
      message = Message.from("Reservation range is too long. Maximum booked days is 3.");
    }

    if(!departureDateIsAfterArrivalDate(reservationRequest)) {
      message = Message.from("Invalid departure date. It must occur after the chosen arrival date.");
    }

    return Optional.ofNullable(message);
  }

  private boolean arrivalDateIsAfterToday(ReservationRequest reservationRequest) {
    return reservationRequest.getArrivalDate().isAfter(today());
  }

  private boolean arrivalDateIsLatestAMonthFromNow(ReservationRequest reservationRequest) {
    return !reservationRequest.getArrivalDate().isAfter(oneMonthIntoFuture());
  }

  private boolean lengthOfReservationIsMaxThreeDays(ReservationRequest reservationRequest) {
    return !reservationRequest.getDepartureDate().minusDays(3).isAfter(reservationRequest.getArrivalDate());
  }

  private boolean departureDateIsAfterArrivalDate(ReservationRequest reservationRequest) {
    return reservationRequest.getDepartureDate().isAfter(reservationRequest.getArrivalDate());
  }
}
