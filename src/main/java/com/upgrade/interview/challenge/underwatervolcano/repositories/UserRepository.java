package com.upgrade.interview.challenge.underwatervolcano.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.upgrade.interview.challenge.underwatervolcano.entities.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
  Optional<User> findUserByEmailAndFirstNameAndLastName(String email, String firstName, String lastName);
}
