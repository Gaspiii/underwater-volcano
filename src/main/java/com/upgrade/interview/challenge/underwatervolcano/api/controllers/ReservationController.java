package com.upgrade.interview.challenge.underwatervolcano.api.controllers;

import java.net.URI;
import java.time.LocalDate;
import java.util.Optional;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.upgrade.interview.challenge.underwatervolcano.api.models.Message;
import com.upgrade.interview.challenge.underwatervolcano.api.models.ReservationRequest;
import com.upgrade.interview.challenge.underwatervolcano.api.models.ReservationResponse;
import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;
import com.upgrade.interview.challenge.underwatervolcano.services.ReservationRequestService;
import com.upgrade.interview.challenge.underwatervolcano.services.ReservationService;

@RestController
@RequestMapping(path = "/reservation", produces = "application/json")
@RequiredArgsConstructor
public class ReservationController {
  @NonNull
  private final ReservationService reservationService;

  @NonNull
  private final ReservationRequestService reservationRequestService;

  @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
  @ApiOperation(value = "Books a new reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 201, response = ReservationResponse.class, message = "Successfully booked reservation."),
      @ApiResponse(code = 200, response = Message.class, message = "Date already reserved."),
      @ApiResponse(code = 400, response = Message.class, message = "Invalid arrival and/or departure date."),
  })
  public ResponseEntity book(@RequestBody() ReservationRequest reservationRequest) {
    Optional<Message> invalidRequestMessage = reservationRequestService.getMessageIfInvalid(reservationRequest);
    if(invalidRequestMessage.isPresent()) {
      return ResponseEntity.badRequest().body(invalidRequestMessage);
    }

    if(!reservationService.isBookable(reservationRequest.getArrivalDate(), reservationRequest.getDepartureDate())) {
      return nonBookableDates();
    }

    Optional<Reservation> reservation = reservationService.book(reservationRequest.getEmail(), reservationRequest.getFirstName(), reservationRequest.getLastName(),
        reservationRequest.getArrivalDate(), reservationRequest.getDepartureDate());

    if(reservation.isEmpty()) {
      return nonBookableDates();
    }
    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}")
        .buildAndExpand(reservation.get().getId())
        .toUri();

    return ResponseEntity.created(location).body(new ReservationResponse(reservation.get().getId()));
  }

  @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", value = "/{id}")
  @ApiOperation(value = "Updates an existing reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 200, response = ReservationResponse.class, message = "Successfully updated reservation."),
      @ApiResponse(code = 200, response = Message.class, message = "Unable to find reservation with given ID."),
      @ApiResponse(code = 200, response = Message.class, message = "Unable to update due to date already being reserved."),
      @ApiResponse(code = 400, response = Message.class, message = "Invalid arrival and/or departure date."),
  })
  public ResponseEntity update(@PathVariable("id") Long id, @RequestBody() ReservationRequest reservationRequest) {
    Optional<Message> invalidRequestMessage = reservationRequestService.getMessageIfInvalid(reservationRequest);
    if(invalidRequestMessage.isPresent()) {
      return ResponseEntity.badRequest().body(invalidRequestMessage);
    }

    Optional<Reservation> reservation = reservationService.get(id);
    if(reservation.isEmpty()) {
      return reservationNotFound();
    }

    if(!reservationService.isBookable(reservationRequest.getArrivalDate(), reservationRequest.getDepartureDate(), id)) {
      return nonBookableDates();
    }

    Optional optional = reservationService.update(reservation.get(), reservationRequest.getEmail(), reservationRequest.getFirstName(), reservationRequest.getLastName(),
        reservationRequest.getArrivalDate(), reservationRequest.getDepartureDate());
    if(optional.isEmpty()) {
      return nonBookableDates();
    }

    return ResponseEntity.ok().body(new ReservationResponse(id));
  }

  @ApiOperation(value = "Retrieves a reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 200, response = ReservationResponse.class, message = "Successfully retrieved reservation."),
      @ApiResponse(code = 200, response = Message.class, message = "Unable to find reservation with given ID."),
      @ApiResponse(code = 400, response = Message.class, message = "Invalid arrival and/or departure date."),
  })
  @RequestMapping(method = RequestMethod.GET, value = "/{id}")
  public ResponseEntity get(@PathVariable("id") Long id) {
    Optional<Reservation> reservation = reservationService.get(id);

    if(reservation.isEmpty()) {
      return reservationNotFound();
    }

    return ResponseEntity.ok().body(reservation.get());
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
  @ApiOperation(value = "Cancels a reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 200, response = ReservationResponse.class, message = "Successfully cancelled reservation."),
      @ApiResponse(code = 200, response = Message.class, message = "Unable to find reservation with given ID."),
      @ApiResponse(code = 200, response = Message.class, message = "Reservation is in the past."),
      @ApiResponse(code = 400, response = Message.class, message = "Invalid arrival and/or departure date."),
  })
  public ResponseEntity cancel(@PathVariable("id") Long id) {
    Optional<Reservation> reservation = reservationService.get(id);
    if(reservation.isEmpty()) {
      return reservationNotFound();
    }

    if(!reservation.get().getDepartureDate().isAfter(LocalDate.now())) {
      return ResponseEntity.ok().body(Message.from("Reservation already past, therefore it is not deletable."));
    }

    reservationService.delete(id);
    return ResponseEntity.ok().body(Message.from("Reservation was deleted"));
  }

  private static ResponseEntity reservationNotFound() {
    return ResponseEntity.ok().body(Message.from("Cannot find a reservation with the given ID."));
  }

  private static ResponseEntity nonBookableDates() {
    return ResponseEntity.ok().body(Message.from("Reservation not accepted. Date was already pre-reserved."));
  }
}
