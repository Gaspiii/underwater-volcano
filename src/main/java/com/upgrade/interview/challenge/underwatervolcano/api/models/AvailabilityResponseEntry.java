package com.upgrade.interview.challenge.underwatervolcano.api.models;

import static com.upgrade.interview.challenge.underwatervolcano.SwaggerConfig.DATE_TIME_FORMAT;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.NonNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Data
public class AvailabilityResponseEntry {
  @NonNull
  @JsonFormat(pattern = DATE_TIME_FORMAT)
  private LocalDateTime date;

  @NonNull
  private Boolean isAvailable;
}
