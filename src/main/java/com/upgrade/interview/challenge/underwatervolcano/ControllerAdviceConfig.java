package com.upgrade.interview.challenge.underwatervolcano;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class ControllerAdviceConfig {
  @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid input")
  @ExceptionHandler(HttpMessageNotReadableException.class)
  public void handleException(HttpMessageNotReadableException e) {
    log.error("Invalid input from api: ", e);
  }
}
