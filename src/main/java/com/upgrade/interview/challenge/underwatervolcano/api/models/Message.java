package com.upgrade.interview.challenge.underwatervolcano.api.models;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Message {
  private Message(String message) {
    this.message = message;
  }

  @NonNull
  private String message;

  public static Message from(String message) {
    return new Message(message);
  }
}
