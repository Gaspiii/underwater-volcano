package com.upgrade.interview.challenge.underwatervolcano.services;

import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import lombok.NonNull;

import org.springframework.stereotype.Service;

import com.upgrade.interview.challenge.underwatervolcano.entities.User;
import com.upgrade.interview.challenge.underwatervolcano.repositories.UserRepository;

@Service
public class UserService {
  @NonNull
  private final UserRepository userRepository;
  private final Lock lock;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
    this.lock = new ReentrantLock();
  }

  public User getOrCreateUser(String email, String firstName, String lastName) {
    lock.lock();
    User user;
    try {
      Optional<User> userOptional = getUser(email, firstName, lastName);
      if(userOptional.isEmpty()) {
        user = createAndSaveUser(email, firstName, lastName);
      } else {
        user = userOptional.get();
      }
    } finally {
      lock.unlock();
    }
    return user;
  }

  private Optional<User> getUser(String email, String firstName, String lastName) {
    return userRepository.findUserByEmailAndFirstNameAndLastName(email, firstName, lastName);
  }

  private User createUser(String email, String firstName, String lastName) {
    return User.builder()
        .email(email)
        .firstName(firstName)
        .lastName(lastName)
        .build();
  }

  private User createAndSaveUser(String email, String firstName, String lastName) {
    User user = createUser(email, firstName, lastName);
    return userRepository.save(user);
  }
}
