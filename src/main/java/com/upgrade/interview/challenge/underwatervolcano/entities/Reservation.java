package com.upgrade.interview.challenge.underwatervolcano.entities;

import static com.upgrade.interview.challenge.underwatervolcano.SwaggerConfig.DATE_FORMAT;

import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@ToString
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(indexes = {@Index (name = "idx_arrival_date", columnList = "arrivalDate"), @Index (name = "idx_departure_date", columnList = "departureDate")})
public class Reservation {
  @Id
  @Generated
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;

  @NonNull
  @Setter
  @JsonFormat(pattern = DATE_FORMAT)
  private LocalDate arrivalDate;

  @NonNull
  @Setter
  @JsonFormat(pattern = DATE_FORMAT)
  private LocalDate departureDate;

  @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH})
  @Setter
  private User user;
}
