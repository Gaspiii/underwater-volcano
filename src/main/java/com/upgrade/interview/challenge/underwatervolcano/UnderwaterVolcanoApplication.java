package com.upgrade.interview.challenge.underwatervolcano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnderwaterVolcanoApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnderwaterVolcanoApplication.class, args);
	}

}
