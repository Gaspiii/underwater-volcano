package com.upgrade.interview.challenge.underwatervolcano.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateTimeUtils {
  private DateTimeUtils(){}

  public static LocalDateTime convertToLocalDateTime(LocalDate localDate) {
    return LocalDateTime.of(localDate, LocalTime.NOON);
  }

  public static LocalDate today() {
    return LocalDate.now();
  }

  public static LocalDate tomorrow() {
    return today().plusDays(1);
  }

  public static LocalDate oneMonthIntoFuture() {
    return LocalDate.now().plusMonths(1);
  }
}
