package com.upgrade.interview.challenge.underwatervolcano.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.upgrade.interview.challenge.underwatervolcano.entities.Reservation;
import com.upgrade.interview.challenge.underwatervolcano.entities.User;
import com.upgrade.interview.challenge.underwatervolcano.repositories.ReservationRepository;

@Service
public class ReservationService {
  private ReservationRepository reservationRepository;
  private UserService userService;
  private final Lock saveLock;

  public ReservationService(ReservationRepository reservationRepository, UserService userService) {
    this.reservationRepository = reservationRepository;
    this.userService = userService;
    this.saveLock = new ReentrantLock();
  }

  public Optional<Reservation> book(String email, String firstName, String lastName, LocalDate arrivalDate, LocalDate departureDate) {
    User user = userService.getOrCreateUser(email, firstName, lastName);
    return book(user, arrivalDate, departureDate);
  }

  private Optional<Reservation> book(User user, LocalDate arrivalDate, LocalDate departureDate) {
    return createAndSaveReservation(arrivalDate, departureDate, user);
  }

  public Optional<Reservation> update(Reservation reservationToUpdate, String email, String firstName, String lastName, LocalDate arrivalDate, LocalDate departureDate) {
    reservationToUpdate.setArrivalDate(arrivalDate);
    reservationToUpdate.setDepartureDate(departureDate);

    reservationToUpdate.getUser().setEmail(email);
    reservationToUpdate.getUser().setFirstName(firstName);
    reservationToUpdate.getUser().setLastName(lastName);
    return saveReservation(reservationToUpdate);
  }

  public void delete(Long reservationId) {
    reservationRepository.deleteById(reservationId);
  }

  public Optional<Reservation> get(Long reservationId) {
    return reservationRepository.findById(reservationId);
  }

  public boolean isBookable(LocalDate arrivalDate, LocalDate departureDate) {
    return isBookable(arrivalDate, departureDate, null);
  }

  public boolean isBookable(LocalDate arrivalDate, LocalDate departureDate, Long excludedId) {
    List<Reservation> reservations = reservationRepository.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);
    if(excludedId != null) {
      reservations = reservations.stream().filter(r -> r.getId() != excludedId).collect(Collectors.toList());
    }
    return reservations.isEmpty();
  }

  public List<Reservation> getReservationsWithUnionToBookingDates(LocalDate arrivalDate, LocalDate departureDate) {
    return reservationRepository.findReservationsWithUnionToBookingDates(arrivalDate, departureDate);
  }

  private Optional<Reservation> createAndSaveReservation(LocalDate arrivalDate, LocalDate departureDate, User user) {
    Reservation reservation = Reservation.builder()
        .arrivalDate(arrivalDate)
        .departureDate(departureDate)
        .user(user)
        .build();
    return saveReservation(reservation);
  }

  private Optional<Reservation> saveReservation(Reservation reservation) {
    saveLock.lock();
    Reservation savedReservation = null;
    try{
      boolean isBookable = isBookable(reservation.getArrivalDate(), reservation.getDepartureDate(), reservation.getId());
      if(isBookable) {
        savedReservation = reservationRepository.save(reservation);
      }
    } finally {
      saveLock.unlock();
    }
    return Optional.ofNullable(savedReservation);
  }
}
